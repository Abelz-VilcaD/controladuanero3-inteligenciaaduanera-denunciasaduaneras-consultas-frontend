import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModulesComponent } from './modules.component';

const routes: Routes = [

  {path:'', component: ModulesComponent,
  children:[
    {path: 'denuncia', loadChildren: () => import('./intranet/denuncia/denuncia.module').then(m => m.DenunciaModule)},
    
  ]
  }
  
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
