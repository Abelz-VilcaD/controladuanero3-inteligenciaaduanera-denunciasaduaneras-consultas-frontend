
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {FormBuilder,FormGroup,Validators,FormControl,AbstractControl,ReactiveFormsModule,} from '@angular/forms';
import {  Component,OnInit,ViewChild,ElementRef,TemplateRef,  Output,EventEmitter,Input,AfterViewInit,AfterContentInit,OnChanges,DoCheck,} from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';

import { ConsultaDenunciaService } from 'src/app/services/consulta-denuncia.service';
@Component({
  selector: 'app-resultado-busqueda-investigaciones',
  templateUrl: './resultado-busqueda-investigaciones.component.html',
  styleUrls: ['./resultado-busqueda-investigaciones.component.css']
})
export class ResultadoBusquedaInvestigacionesComponent implements OnInit {
  //MODALES
  modalRef?: BsModalRef;
  modalReporteInvestigacion?: BsModalRef;
  @ViewChild('templateNotaAgente') _templateReporteDenuncia?: TemplateRef<any>;
  @ViewChild('templateNotaInteligencia') _templateNotaInteligencia?: TemplateRef<any>;
  @ViewChild('templatePropuestaInvestigacion') _templatePropuestaInvestigacion?: TemplateRef<any>;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public modalService: BsModalService,
    private ubigeo: ConsultaDenunciaService,) { }

  ngOnInit(): void {
  }
  modalReposrteDenuncia() {
    let objClass = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg',
      scrollable: true,
    };
    this.modalRef = this.modalService.show(
      this._templateReporteDenuncia!,
      objClass
    );
  }
  modalNotaInteligencia() {
    let objClass = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg',
      scrollable: true,
    };
    this.modalRef = this.modalService.show(
      this._templateNotaInteligencia!,
      objClass
    );
  }
  modalPropuestaInvestigacioon() {
    let objClass = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg',
      scrollable: true,
    };
    this.modalRef = this.modalService.show(
      this._templatePropuestaInvestigacion!,
      objClass
    );
  }

}
