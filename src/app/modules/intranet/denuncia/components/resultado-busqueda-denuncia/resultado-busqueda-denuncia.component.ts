import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {FormBuilder,FormGroup,Validators,FormControl,AbstractControl,ReactiveFormsModule,} from '@angular/forms';
import {  Component,OnInit,ViewChild,ElementRef,TemplateRef,  Output,EventEmitter,Input,AfterViewInit,AfterContentInit,OnChanges,DoCheck,} from '@angular/core';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';

import { ConsultaDenunciaService } from 'src/app/services/consulta-denuncia.service';

import { faDownload } from '@fortawesome/free-solid-svg-icons';


import { saveAs } from 'file-saver';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-resultado-busqueda-denuncia',
  templateUrl: './resultado-busqueda-denuncia.component.html',
  styleUrls: ['./resultado-busqueda-denuncia.component.css'],
})
export class ResultadoBusquedaDenunciaComponent implements OnInit {
//icons

@Input() formularioDenuncia:any;

public page?: number;

faDownload=faDownload;

  ubigeo1!: any;
  lstDepartamento: any[] = [];
  lstProvincia: any[] = [];
  lstDistrito: any[] = [];
  resp: any = [];
  lstdenuncia:any;
  
  lstexport:any;

datosform:any;

  //MODALES
  modalRef?: BsModalRef;
  modalReporteDenuncia?: BsModalRef;
  @ViewChild('templateReporteDenuncia')
  _templateReporteDenuncia?: TemplateRef<any>;
 
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public modalService: BsModalService,
    private ubigeo: ConsultaDenunciaService,
    private datepipe: DatePipe,
    
  ) {}

  ngOnInit(): void {
  this.ubigeo.disparadorDenuncia.subscribe(resp=>{
  this.lstdenuncia=resp.resp.listaDenuncias

  console.log('recviendo data',resp)

this.datosform=resp.params;

})


  }


excel(){
  this.exporarexcel(this.datosform)
}

  exporarexcel(params:any){

    this.ubigeo.exportarExcel(params).subscribe(
      (resp) => {
        
       let latest_date = this.datepipe.transform(new Date(), 'dd-MM-yyyy-HHmmss');
        
        let blob = new Blob([resp], {type: 'text/xlsx'});
        var filename = 'reporte_denuncia_' + latest_date + '.xlsx'
        saveAs(blob,filename)
    
      },
      (error) => {
        console.log(error);
      }
    );
  }

  modalReposrteDenuncia() {
    let objClass = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-lg',
      scrollable: true,
    };
    this.modalReporteDenuncia = this.modalService.show(
      this._templateReporteDenuncia!,
      objClass
    );
  }
}
