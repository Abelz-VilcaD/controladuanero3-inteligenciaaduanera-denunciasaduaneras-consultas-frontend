import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ReactiveFormsModule, } from '@angular/forms';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-consulta-investigaciones',
  templateUrl: './consulta-investigaciones.component.html',
  styleUrls: ['./consulta-investigaciones.component.css']
})
export class ConsultaInvestigacionesComponent implements OnInit {
  /** declaracion de variables */
 
  options: any;
  optionss: any;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public modalService: BsModalService, ) { }

  ngOnInit(): void {
  }

  formInvestigacion = new FormGroup({
  añodel: new FormControl('', [Validators.required, Validators.maxLength(6)]),
  añoal: new FormControl('', [Validators.required, Validators.maxLength(6)]),

});

 

}
