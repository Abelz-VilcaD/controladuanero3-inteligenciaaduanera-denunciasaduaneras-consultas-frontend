
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ReactiveFormsModule, } from '@angular/forms';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConsultaDenunciaService } from 'src/app/services/consulta-denuncia.service';
import * as moment from 'moment';
import { saveAs } from 'file-saver';
import { DatePipe } from '@angular/common';
import { NgbDateStruct, NgbDateParserFormatter, NgbAccordionConfig } from '@ng-bootstrap/ng-bootstrap';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { disableDebugTools } from '@angular/platform-browser';

@Component({
  selector: 'app-consulta-denuncia',
  templateUrl: './consulta-denuncia.component.html',
  styleUrls: ['./consulta-denuncia.component.css'],
})
export class ConsultaDenunciaComponent implements OnInit {

  fecha: number = Date.now();
  ubigeo1!: any;
  lstDenuncia: any[] = [];
  lstDepartamento: any[] = [];
  lstProvincia: any[] = [];
  lstDistrito: any[] = [];
  resp: any = [];

  listaubigeo: any = [];

  denuncia: any;

  objForm!: FormGroup;

  formularioDenuncia!: FormGroup;
  bldisable:boolean=false;
  blenable:boolean=true;
 
checkvalueEstado:boolean=false;
checkvalueOrigen:boolean=false;
checkvalueUbigeo:boolean=false;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    public modalService: BsModalService,
    private ubigeo: ConsultaDenunciaService,
    private datepipe: DatePipe
  ) { }

  ngOnInit(): void {

    this.departamentos();
    this.validforms();
    this.desabilitado();
   // this.refrescar();
    //this.desabilitadoadicional()
   }


   public getError(controlName: string): string {
     let error = '';
     const control = this.formularioDenuncia.get(controlName);
     if (control?.touched && control.errors != null) {
       error = JSON.stringify(control.errors);
     }
     return error;
   }
   public register() {
    const user = this.formularioDenuncia.value;
    console.log(user);
  }


  /**** */

habilitarcomboEstado(value:boolean)
{
  this.checkvalueEstado=value
   
}
habilitarcomboOrigen(value:boolean)
{
  this.checkvalueOrigen=value
   
}
habilitarcomboUbigeo(value:boolean)
{
  this.checkvalueUbigeo=value
   
}


 validforms(){
    const minañoLength = 4;
  this.formularioDenuncia = this.formBuilder.group({
    
    denunciadel: ['', [Validators.required]],

    denunciaal: ['', [Validators.required,Validators.minLength(minañoLength)]],
    denunciahasta: ['', [Validators.required,]],
    denunciaaño: ['', [Validators.required, this.validaraño.bind(this)]],
    denunciadesde: ['', [Validators.required]],
    denunciaestado:['',[Validators.required]],
    denunciaorigen:['',[Validators.required]],
    selectdep:['',[Validators.required]],
    selectprov:['',[Validators.required]],
    selectdist:['',[Validators.required]],
  });
}

departamentos(){

//  this.formularioDenuncia.controls['Ddesde'].disable();
  this.ubigeo.accderUbigeo().subscribe(
    (resp) => {
      this.ubigeo1 = resp;
      this.lstDepartamento = this.filtrardep(this.ubigeo1.ubigeos);
    },
    (error) => {
      console.log(error);
    }
  );
}

  validaraño(control: AbstractControl): any {
    const formGroup = control.parent;
    if (formGroup) {
      const año = formGroup.get('denunciaaño')?.value; // to get value in input tag
      const añoactual = moment(this.fecha).format('yyyy');


      if (año<añoactual) {
      
          return { matchaño: true };
          
        } else {
          return null;
        }
      
    }
  }





  desabilitado() {      
    this.formularioDenuncia.get('denunciadesde')?.disable();
    this.formularioDenuncia.get('denunciahasta')?.disable(); 
    this.formularioDenuncia.get('denunciaaño')?.disable();
    this.formularioDenuncia.get('denunciadel')?.disable(); 
    this.formularioDenuncia.get('denunciaal')?.disable();  
    
   
  }
  habilitadoradiofecha() {      
    this.formularioDenuncia.get('denunciadesde')?.enable();
    this.formularioDenuncia.get('denunciahasta')?.enable(); 
   this.formularioDenuncia.get('denunciaaño')?.disable();
    this.formularioDenuncia.get('denunciadel')?.disable(); 
     this.formularioDenuncia.get('denunciaal')?.disable();  
   
  }

  habilitadoradiodenuncia(){ 
    this.formularioDenuncia.get('denunciadesde')?.disable();
    this.formularioDenuncia.get('denunciahasta')?.disable();
    this.formularioDenuncia.get('denunciaaño')?.enable();
    this.formularioDenuncia.get('denunciadel')?.enable(); 
    this.formularioDenuncia.get('denunciaal')?.enable(); 

    // this.formularioDenuncia = this.formBuilder.group({
    //   Ddel: ['', ],
    //   Dal: ['', ],
    //   Dhasta: [{value: '', disabled: false}],
    //   Daño: ['',],
    //   Ddesde: [{value: '', disabled: false}],
        
    //   });     
  }

  refrescar() {
    this.formularioDenuncia = this.formBuilder.group({
      denunciadel: '',
      denunciaal: '',
      denunciahasta: '',
      denunciaaño: [''],
      denunciadesde: '',
    });
  }

  filtrardep(lst: any[]): any[] {
    let lst1: any[] = [];
    for (let i = 0; i < lst.length; i++) {
      const element = lst[i].desDepartamento;
      if (!lst1.includes(lst[i].desDepartamento)) {
        lst1.push(element);
      }
    }
    return lst1;
  }
  filtrarprov(lst: any[]): any[] {
    let lst1: any[] = [];
    for (let i = 0; i < lst.length; i++) {
      const element = lst[i].desProvincia;
      if (!lst1.includes(lst[i].desProvincia)) {
        lst1.push(element);
      }
    }
    return lst1;
  }
  filtrardist(lst: any[]): any[] {
    let lst1: any[] = [];
    for (let i = 0; i < lst.length; i++) {
      const element = lst[i].desDistrito;
      if (!lst1.includes(lst[i].desDistrito)) {
        lst1.push(element);
      }
    }
    return lst1;
  }

  onChangeDep(event: any) {
    this.listaubigeo = this.ubigeo1.ubigeos;
    this.lstProvincia = this.filtrarprov(
      this.listaubigeo.filter(
        (x: any) => x.desDepartamento == event.target.value
      )
    );
    // alert(event.target.value);
    //console.log(this.lstProvincia)
  }
  onChangeProv(event: any) {
    this.listaubigeo = this.ubigeo1.ubigeos;
    this.lstDistrito = this.filtrardist(
      this.listaubigeo.filter((x: any) => x.desProvincia == event.target.value)
    );
    // alert(event.target.value);
    console.log(this.lstDistrito);
  }

  public obtnerlIstaDenuncia() {
    let objForm = this.formularioDenuncia;

    /*const valuedesde = objForm.get('desde')?.value;*/

    const valuedesde = moment(objForm.get('denunciadesde')?.value).format('DD/MM/yyyy');
    const valuehasta = moment(objForm.get('denunciahasta')?.value).format('DD/MM/yyyy');
    let params = {
      idPto: '',
      idDist: '',
      idProv: '',
      codTipoOrigen: '',
      codEstDenuncia: '',
      fecEmisionIni: valuedesde,
      fecEmisionFin: valuehasta,
      annDenuncia: '',
      numFin: '',
      numIni: '',
    };

    this.ubigeo.$accederDenuncia(params).subscribe(
      (resp) => {
        this.denuncia = resp;
        this.lstDenuncia = resp;

        console.log(this.lstDenuncia);
        this.ubigeo.disparadorDenuncia.emit({
          resp,
          params,
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  public pruebaexcel() {
    // let queryParameters= HttpParams();
    let objForm = this.formularioDenuncia;
    const valuedesde = moment(objForm.get('denunciadesde')?.value).format('DD/MM/yyyy');
    const valuehasta = moment(objForm.get('denunciahasta')?.value).format('DD/MM/yyyy');

    let params = {
      idPto: '',
      idDist: '',
      idProv: '',
      codTipoOrigen: '',
      codEstDenuncia: '',
      fecEmisionIni: valuedesde,
      fecEmisionFin: valuehasta,
      annDenuncia: '',
      numFin: '',
      numIni: '',
    };
    //console.log(params);
    this.ubigeo.exportarExcel(params).subscribe(
      (resp) => {
        let latest_date = this.datepipe.transform(
          new Date(),
          'dd-MM-yyyy-HHmmss'
        );

        let blob = new Blob([resp], { type: 'text/xlsx' });
        var filename = 'reporte_denuncia_' + latest_date + '.xlsx';
        saveAs(blob, filename);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}