import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultaDatosAnalisisComponent } from './components/consulta-datos-analisis/consulta-datos-analisis.component';
import { ConsultaDenunciaComponent } from './components/consulta-denuncia/consulta-denuncia.component';
import { ConsultaInvestigacionesComponent } from './components/consulta-investigaciones/consulta-investigaciones.component';
import { PruebaComponent } from './components/prueba/prueba.component';
import { ResultadoBusquedaDenunciaComponent } from './components/resultado-busqueda-denuncia/resultado-busqueda-denuncia.component';
import { ResultadoBusquedaInvestigacionesComponent } from './components/resultado-busqueda-investigaciones/resultado-busqueda-investigaciones.component';

const routes: Routes = [
  {path: 'consulta-denuncia', component: ConsultaDenunciaComponent},
  {path: 'consulta-investigacion', component: ConsultaInvestigacionesComponent},
  {path: 'consulta-datos-analisis', component:  ConsultaDatosAnalisisComponent},
  {path: 'resultado-busqueda', component:  ResultadoBusquedaDenunciaComponent},
  {path: 'resultado-busqueda-investigacion', component:  ResultadoBusquedaInvestigacionesComponent},
  {path: 'prueba', component: PruebaComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DenunciaRoutingModule { }
