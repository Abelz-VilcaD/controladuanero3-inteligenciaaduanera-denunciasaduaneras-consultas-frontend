import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DenunciaRoutingModule } from './denuncia-routing.module';
import { ConsultaDenunciaComponent } from './components/consulta-denuncia/consulta-denuncia.component';
import { ConsultaInvestigacionesComponent } from './components/consulta-investigaciones/consulta-investigaciones.component';
import { ConsultaDatosAnalisisComponent } from './components/consulta-datos-analisis/consulta-datos-analisis.component';
import { ResultadoBusquedaDenunciaComponent } from './components/resultado-busqueda-denuncia/resultado-busqueda-denuncia.component';
import { AppMaterialModule } from 'src/app/app-material.module';
import { AppPrimengModule } from 'src/app/app-primeng.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ResultadoBusquedaInvestigacionesComponent } from './components/resultado-busqueda-investigaciones/resultado-busqueda-investigaciones.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { PruebaComponent } from './components/prueba/prueba.component';
@NgModule({
  declarations: [
    ConsultaDenunciaComponent,
    ConsultaInvestigacionesComponent,
    ConsultaDatosAnalisisComponent,
    ResultadoBusquedaDenunciaComponent,
    ResultadoBusquedaInvestigacionesComponent,
    PruebaComponent
  ],
  imports: [
 
    CommonModule,
    DenunciaRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AppPrimengModule,
    AppMaterialModule,
    HttpClientModule,
    NgbModule,
    ModalModule,
    FontAwesomeModule,
    NgxPaginationModule
  ]
})
export class DenunciaModule { }
