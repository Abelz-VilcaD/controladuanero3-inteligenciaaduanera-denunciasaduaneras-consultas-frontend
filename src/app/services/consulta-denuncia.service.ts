import { EventEmitter, Injectable, Output} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ConsultaDenunciaService {
//cmunicar entre componetes
  @Output() disparadorDenuncia :EventEmitter<any>=new EventEmitter ();


  
  private apiUrlUbideo = environment.urlBase;
  private apiUrl=environment.URL_HOST;
  private apiurlDenuncia = '/controladuanero/consultasaduaneras/t/denuncias/?'
  private apiUrlExcel='/controladuanero/consultasaduaneras/t/denuncias/exportarexcelconsulta/?';
  constructor(private http: HttpClient) { }

  public accderUbigeo(): Observable<any>{
  
    return this.http.get<any>(this.apiUrlUbideo +'/v1/estrategico/aduanero/e/ubigeos');
    
   }

   public $accederDenuncia(params:any): Observable<any>{
 
 
    let queryParameters= new HttpParams();
    queryParameters=queryParameters.append('idPto','');
    queryParameters=queryParameters.append('idDist','');
    queryParameters=queryParameters.append('idProv','');
    queryParameters=queryParameters.append('codTipoOrigen','');
    queryParameters=queryParameters.append('codEstDenuncia','');
    queryParameters=queryParameters.append('fecEmisionIni',params.fecEmisionIni);
    queryParameters=queryParameters.append('fecEmisionFin', params.fecEmisionFin);
    queryParameters=queryParameters.append('annDenuncia','');
    queryParameters=queryParameters.append('numFin','');
    queryParameters=queryParameters.append('numIni','');
  
    return this.http.get<any>(this.apiUrl + this.apiurlDenuncia+queryParameters);
    
   }

   public exportarExcel(params:any):Observable<any>{

    let queryParameters= new HttpParams();
    queryParameters=queryParameters.append('idPto','');
    queryParameters=queryParameters.append('idDist','');
    queryParameters=queryParameters.append('idProv','');
    queryParameters=queryParameters.append('codTipoOrigen','');
    queryParameters=queryParameters.append('codEstDenuncia','');
    queryParameters=queryParameters.append('fecEmisionIni',params.fecEmisionIni);
    queryParameters=queryParameters.append('fecEmisionFin', params.fecEmisionFin);
    queryParameters=queryParameters.append('annDenuncia','');
    queryParameters=queryParameters.append('numFin','');
    queryParameters=queryParameters.append('numIni','');

    return this.http.get(this.apiUrl + this.apiUrlExcel+queryParameters, {
      params: queryParameters,
      responseType: 'blob',
      //type: 'application/vnd.ms-excel'

  });

   }

}
