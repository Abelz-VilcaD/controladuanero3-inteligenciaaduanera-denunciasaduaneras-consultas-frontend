export class CriterioPonderacion {
  numeroCriterio!: number;
  nombreCriterio!: string;
  elementos!: ElementoCriterioPonderacion[];
}
class ElementoCriterioPonderacion {
  id!: number;
  descripcion!: string;
  origenWebAnonima!: number;
  origenWebNoAnonima!: number;
  origenIntranetAnonima!: number;
  origenIntranetNoAnonima!: number;
}
