export class ContenidoArchivo {
  nombreArchivo?: string;
  detalle?: string;
  tamanioKb?: number;
  tamanioMb?: number;
  base64?: string;
  blob?: string;
  file?: File;
}
