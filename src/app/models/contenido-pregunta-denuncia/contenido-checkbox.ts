export class ContenidoCheckbox {
  nombre?: string;
  descripcion?: string;
  seleccionado?: boolean;
  contenido?: SubContenidoCheckBox[];
}

export class SubContenidoCheckBox {
  descripcion?: string;
  seleccionado?: boolean;
}
