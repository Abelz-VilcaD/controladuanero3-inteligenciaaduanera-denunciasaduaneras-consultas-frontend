import { Departamento } from "../departamento";
import { Distrito } from "../distrito";
import { Provincia } from "../provincia";
import { TipoVia } from "../tipo-via";

export class ContenidoDireccion {
  departamento?: Departamento;
  provincia?: Provincia;
  distrito?: Distrito;
  tipoVia?:TipoVia;
  ubigeo?: string;
  direccion?: string;
}
