import { TipoDocumento } from "../tipo-documento";

export class ContenidoDocumento {
  tipoDoc?: TipoDocumento;
  nroDoc?: string;
  fechaDoc?: string;
  anio?: string;
  conoceDatos?:boolean;
}
