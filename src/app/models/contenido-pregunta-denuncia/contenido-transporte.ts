import { MedioTransporte } from "../medio-transporte";
import { Pais } from "../pais";

export class ContenidoTransporte {
  pais?: Pais;
  medioTransporte?: MedioTransporte;
  placa1?: string;
  placa2?: string;
  placa3?: string;
}
