import { ContenidoArchivo } from './contenido-pregunta-denuncia/contenido-archivo';
import { ContenidoCheckbox } from './contenido-pregunta-denuncia/contenido-checkbox';
import { ContenidoDireccion } from './contenido-pregunta-denuncia/contenido-direccion';
import { ContenidoDocumento } from './contenido-pregunta-denuncia/contenido-documento';
import { ContenidoTransporte } from './contenido-pregunta-denuncia/contenido-transporte';
import { TelefonoDenunciaForm } from './telefono-denuncia-form';

export class DatosDenunciaForm {
  pregunta1!: Pregunta<ContenidoCheckbox[]>;
  pregunta2!: Pregunta<string[]>;
  pregunta3!: Pregunta<ContenidoDocumento[]>;
  pregunta4!: Pregunta<ContenidoDireccion[]>;
  pregunta5!: Pregunta<string>;
  pregunta6!: Pregunta<ContenidoFecha>;
  pregunta7!: Pregunta<ContenidoTransporte[]>;
  pregunta8!: Pregunta<TelefonoDenunciaForm[]>;
  pregunta9!: Pregunta<string>;
  pregunta10!: Pregunta<ContenidoEntidad>;
  pregunta11!: Pregunta<ContenidoEntidad>;
  pregunta12!: Pregunta<string>;
  pregunta13!: Pregunta<ContenidoArchivo[]>;
  pregunta14!: Pregunta<TelefonoDenunciaForm[]>;
}

class Pregunta<T> {
  numero?: number;
  pregunta?: string;
  respuesta?: boolean;
  contenido?: T;
  mensajeError?:string='';
}

class ContenidoFecha {
  fechaInicio?: Date;
  fechaFin?: Date;
}

class ContenidoEntidad {
  tipoDocId?: number | string;
  nroDoc?: string;
  nombre?: string;
}
