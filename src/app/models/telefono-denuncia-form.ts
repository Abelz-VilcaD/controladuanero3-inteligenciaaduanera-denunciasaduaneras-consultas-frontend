import { TipoTelefono } from "./tipo-telefono";

export class TelefonoDenunciaForm {
  tipo?: TipoTelefono;
  codigo?: string;
  numero?: number;
}
